<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    protected $role;
    protected $permission;

    public function __construct(Role $role, Permission $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->role->all();

        return view('role.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = $this->permission->all();

        return view('role.add', [
            'permissions' => $permissions
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = $this->role->create([
            'name'=>$request->name,
            'display_name'=>$request->display_name,
        ]);

       $role->permissions()->attach($request->permission);

        return redirect()->route('role.index')->with('success','successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $role = $this->role->findOrFail($id);
        $permissions = $this->permission->all();
        $listPermission = DB::table('role_permissions')->where('role_id',$id)
            ->pluck('permission_id');

        return view('role.edit', compact('role','permissions','listPermission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $this->role->where('id',$id)->update([
                'name' => $request->name,
                'display_name' => $request->display_name,
            ]);
            DB::table('role_permissions')->where('role_id',$id)->delete();
            $role = $this->role->findOrFail($id);
            $role->permissions()->attach($request->permission);
            return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles= Role::find($id);
        $roles-> delete();
        return redirect()->route('role.index');
    }
}
