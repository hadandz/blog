<?php

namespace App\Http\Controllers;


use App\Role;
use App\User;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    private $user;
    private $role;
    public function __construct(User $user, Role $role)
    {
        $this->user=$user;
        $this->role=$role;
    }

    public function index(){
        $users = $this->user->all();
        return view('user.index', compact('users'));
    }

    public function create()
    {
        $roles = $this->role->all();
        return view('user.add', compact('roles'));
    }

    public function store(Request $request){

        $userCreate = $this->user->create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
        ]);

        $roles = $request->roles;
        foreach ($roles as $role){
            \DB::table('role_user')->insert([
                'user_id'=>$userCreate->id,
                'role_id'=>$role,
            ]);
        }
        return redirect()->route('user.index');
    }

    public function edit($id){
        $roles = $this ->role->all();
        $user = $this->user->findOrfail($id);
        $listRoleOfUser = DB::table('role_user')->where('user_id',$id)->pluck('role_id');
        return view('user.edit',compact('roles','user','listRoleOfUser'));
    }

    public function update(Request $request,$id){
        $data = [
            'name' => $request->input('name'),
        ];
        $roleIds = $request->input('checkbox');
        $user =  $this->user->findOrFail($id);
        $user->update($data);
        $user->roles()->detach();
        $user->roles()->attach($roleIds);


        return redirect()->route('user.index')->with('success','repaired successfully');
    }

    public function destroy($id){
        $user= User::find($id);
        $user-> delete();
        return redirect()->route('user.index');
    }



}
