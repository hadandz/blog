@extends('layouts.master')
@section('content')
    <!-- Topbar Search -->
    <nav class=" navbar-light bg-light">
        <form id="form_search">
            <div class="row">
                <div class="col-3">
                    <select id="category_id" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-9">
                    <input class="form-control ml-sm-2" id="search" type="search" placeholder="Search"
                           aria-label="Search" data-href="{{ route('product.search') }}">
                </div>
            </div>
        </form>
    </nav>
    <div class="data">
        <div class="table-responsive my-4 load_data" >
            <div>
                <button type="button" class="btn btn-primary" data-toggle="modal"
                           data-target="#modal_product_create" id="btn_create">
                    Create
                </button>
                @include('product.create')
            </div>

            <table class="table table-hover" id="tbl_product_result">
                @include('product.list')
            </table>
        </div>
    </div>

@endsection
@section('script')
    <script src="js/product.js"></script>
@endsection

