@extends('layouts.master')
@section('content')
    <div class="table-responsive">
        <table class="table table-hover">
            <thead class="thead-light">
            <a href="{{ route('category.create') }}" type="button" class="btn btn-primary">Create</a>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Category Name</th>
                <th scope="col" colspan="2"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $category->name }}</td>
                    <td>
                        <a href="{{ route('category.edit',$category->id) }}"
                           type="button" class="btn btn-success btn-sm">Edit</a>
                    </td>
                    <td>
                        <a href="{{route('category.destroy',$category->id)}}"
                           type="button" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

