@extends('layouts.master')
@section('content')
    <div>
        <form method="POST" action="{{ route('role.update',$role->id) }}">
            @csrf
            <div class="form-group">
                <label>Permission Name</label>
                <input type="text" name="name" class="form-control" value="{{ $role->name }}">
            </div>
            <div class="form-group">
                <label>Display Name</label>
                <input type="text" name="display_name" class="form-control" value="{{ $role->display_name }}">
            </div>
            <div>
                <p>Permission</p>
                <div class="form-group ">
                    @foreach($permissions as $permission)
                        <div class="form-group">
                            <div class="custom-control custom-checkbox  mr-5">
                                <input
                                    {{ $listPermission->contains($permission->id) ? 'checked' : '' }}
                                    type="checkbox" name="permission[]" value="{{ $permission->id }}"
                                       class="custom-control-input" id="role{{ $permission->id }}">
                                <label for="role{{ $permission->id }}"
                                       class="custom-control-label">{{ $permission->display_name }}</label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
