@extends('layouts.master')
@section('content')

    <div class="table-responsive">
        <table class="table table-hover">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">
                    <a href="{{ route('user.create') }}" type="button" class="btn btn-primary">Create</a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <a href="{{ route('user.edit',$user->id) }}"
                           type="button" class="btn btn-success btn-sm">Edit</a>
                        <a href="{{route('user.destroy',$user->id)}}"
                           type="button" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
